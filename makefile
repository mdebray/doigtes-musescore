doigtes_%.qml : src/doigtes instru/eupho src/text_size
	sed -e '/%%doigtes%%/r instru/$*' -e '/%%doigtes%%/d' -e '/%%text_size%%/r src/text_size' -e '/%%text_size%%/d' src/doigtes > $@
	sed -i 's/%%instru%%/$*/g' $@
