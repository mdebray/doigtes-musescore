# Plugin Musescore pour les doigtés des instruments

Pour l'instant seuls l'euphonium, le trombone et la trompette (sans garantie) sont disponible. 

## Utilisation

1. Ouvrez un livre de lecture de notes
2. Apprenez à lire la musique

En vrai je crois que c'est important d'apprendre à lire la musique d'où ce troll.
Mais pour ceux qui en on marre de réfléchir à leurs doigtés voici:

1. Copiez le fichier `.qml` dans le répertoire `$MUSESCORE_DIRECTORY/plugins`
2. Activez le plugin en cochant la case dans le menu `Plugins > Gestionnaire de plugins` (il est possible qu'il faille cliquer sur `Recharger la plugins`)
3. Sur la partition sélectionnez la zone de la portée pour laquelle vous voulez des doigtés (Il faut qu'un carré bleu s'affiche, sinon le plugin va opérer sur toutes les portées).
4. Allez dans le menu `Plugins` et choisissez le plugin `doigtes_{nom de l'instrument}`

## Réutilisation

Le script s'adapte facilement pour d'autres instruments (/!\ les notes sont en si bémol dans les commentaires et les numéros d'octave sont approximatifs)

## TODO

 - Ajout des notes en ut dans les commentaires du tableau `doigtes` et des numéros d'octave corrects
 - Ajouter une option de placement des textes
