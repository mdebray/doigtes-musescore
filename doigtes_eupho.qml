//=============================================================================
//  Doigtés eupho
//
//  This is a modified version of the `Note Names Plugin` musescore
//  default plugin. You can redistribute it and/or modify
//  it under the terms of the GNU General Public License version 2
//  as published by the Free Software Foundation
//
//    For the `Note Names Plugin`:
//  Copyright (C) 2012 Werner Schweer
//  Copyright (C) 2013 - 2020 Joachim Schmitz
//  Copyright (C) 2014 Jörn Eichler
//  Copyright (C) 2020 Johan Temmerman
//    For this plugin:
//  Copyright (C) 2021 Maurice Debray
//
//=============================================================================

import QtQuick 2.2
import MuseScore 3.0

MuseScore {
   version: "2.0"
   description: "Plugin pour mettre les doigtés de eupho sur les portée"
   menuPath: "Plugins.Doigtés eupho"

   property var fontSizeMini: 0.8 //Taille du texte. 1 => Taille normale, 0.5 => Petite taille

   readonly property var doigtes : [ // Les notes sont en sib
      "", //D (-1)
      "", //D#(-1)
      "", //E (-1)
      "", //F (-1)
      "", //F#(-1)
      "", //G (-1)
      "", //G#(-1)
      "", //A (-1)
      "", //A#(-1)
      "", //B (-1)
      "", //C (-1)
      "", //C#(-1)
      "", //D (0)
      "", //D#(0)
      "", //E (0)
      "", //F (0)
      "", //F#(0)
      "", //G (0)
      "", //G#(0)
      "", //A (0)
      "", //A#(0)
      "", //B (0)
      "", //C (0)
      "", //C#(0)
      "", //D (1)
      "", //D#(1)
      "", //E (1)
      "", //F (1)
      "", //F#(1)
      "", //G (1)
      "", //G#(1)
      "", //A (1)
      "", //A#(1)
      "", //B (1)
      "0", //C (1) Première harmonique (très grave)
      "4123", //C#(1)
      "413", //D (2)
      "423", //D#(2)
      "412", //E (2)
      "41", //F (2)
      "42", //F#(2)
      "4", //G (2) Sol grave
      "23", //G#(2)
      "12", //A (2)
      "1", //A#(3)
      "2", //B(3)
      "0", //C (3) Deuxième harmonique Do grave
      "123", //C#(3)
      "4", //D (3)
      "23", //D#(3)
      "12", //E (3)
      "1", //F (3)
      "2", //F#(3)
      "0", //G (3) Troisième harmonique Sol médium
      "23", //G#(3)
      "12", //A (3)
      "1", //A#(4)
      "2", //B(4)
      "0", //C (4) Quatrième harmonique Do aigu
      "12", //C#(4)
      "1", //D (4)
      "2", //D#(4)
      "0", //E (4)
      "1", //F (4)
      "2", //F#(4)
      "0", //G (4)
      "23", //G#(4)
      "12", //A (4)
      "", //A#(4)
      "", //B (4)
      "", //C (4)
      "", //C#(4)
      "", //D (5)
      "", //D#(5)
      "", //E (5)
      "", //F (5)
      "", //F#(5)
      "", //G (5)
      "", //G#(5)
      "", //A (5)
      "", //A#(5)
      "", //B (5)
      "", //C (5)
      "", //C#(5)
      "", //D (6)
      "", //D#(6)
      "", //E (6)
      "", //F (6)
      "", //F#(6)
      "", //G (6)
      "", //G#(6)
      "", //A (6)
      "", //A#(6)
      "", //B (6)
      "", //C (6)
      "", //C#(6)
      "", //D (7)
      "", //D#(7)
      "", //E (7)
      "", //F (7)
      "", //F#(7)
      "", //G (7)
      "", //G#(7)
      "", //A (7)
      "", //A#(7)
      "", //B (7)
      "", //C (7)
      "", //C#(7)
      "", //D (8)
      "", //D#(8)
      "", //E (8)
      "", //F (8)
      "", //F#(8)
      "", //G (8)
      "", //G#(8)
      "", //A (8)
      "", //A#(8)
      "", //B (8)
      "", //C (8)
      "", //C#(8)
      "", //D (9)
      "", //D#(9)
      "", //E (9)
      "", //F (9)
      "", //F#(9)
      "", //G (9)
      "", //G#(9)
      "", //A (9)
      "", //A#(9)
      "", //B (9)
      "", //C (9)
      "", //C#(9)
   ]

   function nameChord (notes, text, small) {
      for (var i = 0; i < notes.length; i++) {
         var sep = "\n";   // change to "," if you want them horizontally (anybody?)
         if ( i > 0 )
            text.text = sep + text.text; // any but top note
         if (small)
             text.fontSize *= fontSizeMini
	 text.text += doigtes[notes[i].pitch]
      }  // end for note
   }

   onRun: {
      var cursor = curScore.newCursor();
      var startStaff;
      var endStaff;
      var endTick;
      var fullScore = false;
      cursor.rewind(1);
      if (!cursor.segment) { // no selection
         fullScore = true;
         startStaff = 0; // start with 1st staff
         endStaff  = curScore.nstaves - 1; // and end with last
      } else {
         startStaff = cursor.staffIdx;
         cursor.rewind(2);
         if (cursor.tick === 0) {
            // this happens when the selection includes
            // the last measure of the score.
            // rewind(2) goes behind the last segment (where
            // there's none) and sets tick=0
            endTick = curScore.lastSegment.tick + 1;
         } else {
            endTick = cursor.tick;
         }
         endStaff = cursor.staffIdx;
      }
      console.log(startStaff + " - " + endStaff + " - " + endTick)

      for (var staff = startStaff; staff <= endStaff; staff++) {
         for (var voice = 0; voice < 4; voice++) {
            cursor.rewind(1); // beginning of selection
            cursor.voice    = voice;
            cursor.staffIdx = staff;

            if (fullScore)  // no selection
               cursor.rewind(0); // beginning of score
            while (cursor.segment && (fullScore || cursor.tick < endTick)) {
               if (cursor.element && cursor.element.type === Element.CHORD) {
                  var text = newElement(Element.STAFF_TEXT);      // Make a STAFF_TEXT
                  var notes = cursor.element.notes;
                  nameChord(notes, text, true); // Set third argument to false to have bigger annotations
                  cursor.add(text);

                  switch (cursor.voice) {
                     case 1: case 3: text.placement = Placement.BELOW; break;
                  }
               } // end if CHORD
               cursor.next();
            } // end while segment
         } // end for voice
      } // end for staff
      Qt.quit();
   } // end onRun
}
